package com.example.navleenkaur.game2;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Handler;


public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "Joust";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    Paint paintbrush;
    private SurfaceHolder holder;


    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

     //VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    // ----------------


    boolean gameOver = false;

    Sprite player;
    Sprite enemy;
    Sprite enemy1;
    Sprite enemy2;
    Sprite enemy3;
    Sprite ground;
    Sprite line1;
    Sprite line2;
    Sprite line3;
    Sprite line4;

    //ArrayList<Player> bullets = new ArrayList<Player>();

    // GAME STATS
    int score = 0;
    int lives = 4;
    private int Distance_FromWall = 100;


    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // PLAYER SETUP
        // ----------------
//        int initialPlayerX = 900;
//        int initialPlayerY = 197;
        //this.player = new Player(context,initialPlayerX, initialPlayerY);

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 50;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites
       this.ground = new Sprite(this.getContext(),0,0, R.drawable.background);
        this.line1 = new Sprite(this.getContext(),0,378, R.drawable.ground);
        this.line2 = new Sprite(this.getContext(),0,756, R.drawable.ground);
        this.line3 = new Sprite(this.getContext(),0,1134, R.drawable.ground);
        this.line4 = new Sprite(this.getContext(),0,1512, R.drawable.ground);

        this.player = new Sprite(this.getContext(), 900, 178, R.drawable.player1);
        this.enemy = new Sprite(this.getContext(), 50, 178, R.drawable.enemy1);
        this.enemy1 = new Sprite(this.getContext(), 150, 556, R.drawable.enemy1);
        this.enemy2 = new Sprite(this.getContext(), 20, 934, R.drawable.enemy1);
        this.enemy3 = new Sprite(this.getContext(), 300, 1312, R.drawable.enemy1);


    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    boolean enemyMoving = true;


    final int SPEED = 10;

    // Game Loop methods
    public void updateGame() {

               // 1. update position of enemy

        if (enemyMoving == true) {
            this.enemy.setxPosition(this.enemy.getxPosition() - SPEED);

            this.enemy.updateHitbox();
            if (this.enemy.getxPosition()< this.VISIBLE_LEFT + Distance_FromWall)
            {
                enemyMoving = false;
            }
        } else {


            this.enemy.setxPosition(this.enemy.getxPosition() + SPEED);

            this.enemy.updateHitbox();
            if(this.enemy.getxPosition()> this.VISIBLE_RIGHT - Distance_FromWall )
            {
                enemyMoving = true;
            }

        }



         if (enemyMoving == true) {
            this.enemy1.setxPosition(this.enemy1.getxPosition() - SPEED);
             this.enemy1.updateHitbox();
             if (this.enemy1.getxPosition()< this.VISIBLE_LEFT + Distance_FromWall)
             {
                 enemyMoving = false;
             }
//
        } else {


            this.enemy1.setxPosition(this.enemy1.getxPosition() + SPEED);
             this.enemy1.updateHitbox();
             if(this.enemy1.getxPosition()> this.VISIBLE_RIGHT - Distance_FromWall )
             {
                 enemyMoving = true;
             }

        }



         if (enemyMoving == true) {
            this.enemy2.setxPosition(this.enemy2.getxPosition() - SPEED);
             this.enemy2.updateHitbox();
             if (this.enemy2.getxPosition()< this.VISIBLE_LEFT + Distance_FromWall)
             {
                 enemyMoving = false;
             }

        } else {


            this.enemy2.setxPosition(this.enemy2.getxPosition() + SPEED);
             this.enemy2.updateHitbox();
             if(this.enemy2.getxPosition()> this.VISIBLE_RIGHT - Distance_FromWall )
             {
                 enemyMoving = true;
             }



        }


         if (enemyMoving == true) {
            this.enemy3.setxPosition(this.enemy3.getxPosition() - SPEED);
             this.enemy3.updateHitbox();
             if (this.enemy3.getxPosition()< this.VISIBLE_LEFT + Distance_FromWall)
             {
                 enemyMoving = false;
             }

        } else {


            this.enemy3.setxPosition(this.enemy3.getxPosition() + SPEED);
             this.enemy3.updateHitbox();

             if(this.enemy3.getxPosition()> this.VISIBLE_RIGHT - Distance_FromWall )
             {
                 enemyMoving = true;
             }


         }




         if (enemyMoving == true) {
            this.player.setxPosition(this.player.getxPosition() + SPEED);
             this.player.updateHitbox();
             if (this.player.getxPosition() > this.VISIBLE_LEFT - Distance_FromWall)
             {
                 enemyMoving = true;
             }
        }
         else {


            this.player.setxPosition(this.player.getxPosition() - SPEED);
             this.player.updateHitbox();
//
             if(this.player.getxPosition()< this.VISIBLE_RIGHT + Distance_FromWall )
             {
                 enemyMoving = false;
             }

        }





         // Collision detection between player and enemy

        if (this.player.getHitbox().intersect(this.enemy.getHitbox()) || this.player.getHitbox() .intersect(this.enemy1.getHitbox())||
                this.player.getHitbox() .intersect(this.enemy2.getHitbox())|| this.player.getHitbox() .intersect(this.enemy3.getHitbox())) {
            Log.d(TAG, "COLLISION!!!!!");
            this.lives = this.lives - 1;
            Log.d(TAG, "Lives remaining: " + this.lives);


            // decide if you should be game over:
            if (this.lives == 0) {
                this.gameOver = true;
                resumeGame();

                return;
            }


            // restart player from starting position
            this.player.setxPosition(900);
            this.player.setyPosition(178);


             //restart hitbox
            Rect hitbox = new Rect(this.player.getxPosition(),
                    this.player.getyPosition(),
                    this.player.getxPosition() + this.player.getImage().getWidth(),
                    this.player.getyPosition() + this.player.getImage().getHeight()
            );

            this.player.setHitbox(hitbox);
        }

//
    }



    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth + "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");

    }


    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
           // canvas.drawColor(Color.argb(255, 255, 255, 255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
           // paintbrush.setStyle(Paint.Style.STROKE);
            //paintbrush.setColor(Color.argb(255, 0, 128, 0));

           // canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            //this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------


            //background
            canvas.drawBitmap(this.ground.getImage(), this.ground.getxPosition(), this.ground.getyPosition(), paintbrush);

            canvas.drawBitmap(this.line1.getImage(), this.line1.getxPosition(), this.line1.getyPosition(), paintbrush);
            canvas.drawBitmap(this.line2.getImage(), this.line2.getxPosition(), this.line2.getyPosition(), paintbrush);
            canvas.drawBitmap(this.line3.getImage(), this.line3.getxPosition(), this.line3.getyPosition(), paintbrush);
            canvas.drawBitmap(this.line4.getImage(), this.line4.getxPosition(), this.line4.getyPosition(), paintbrush);

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. enemy
            canvas.drawBitmap(this.enemy.getImage(), this.enemy.getxPosition(), this.enemy.getyPosition(), paintbrush);
            canvas.drawBitmap(this.enemy1.getImage(), this.enemy1.getxPosition(), this.enemy1.getyPosition(), paintbrush);
            canvas.drawBitmap(this.enemy2.getImage(), this.enemy2.getxPosition(), this.enemy2.getyPosition(), paintbrush);
            canvas.drawBitmap(this.enemy3.getImage(), this.enemy3.getxPosition(), this.enemy3.getyPosition(), paintbrush);


            //canvas.drawRect(1500, 300, 500, 350, paintbrush);
//
            // // setup stroke style and width
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(8);
//
//
//


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect playerHitbox = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(playerHitbox, paintbrush);


            // draw hitbox on enemy
            // --------------------------------------------------------
            Rect enemyHitbox = enemy.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(enemyHitbox, paintbrush);

            // draw hitbox on enemy
            // --------------------------------------------------------
            Rect enemyHitbox2 = enemy1.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(enemyHitbox2, paintbrush);

            // draw hitbox on enemy
            // --------------------------------------------------------
            Rect enemyHitbox3 = enemy2.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(enemyHitbox3, paintbrush);

            // draw hitbox on enemy
            // --------------------------------------------------------
            Rect enemyHitbox4 = enemy3.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(enemyHitbox4, paintbrush);
//

//             ----------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 500,100, paintbrush);

            outputVisibleArea();

            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);  // make text narrow
            canvas.drawText("Lives: " + this.lives, 20, 100, paintbrush);
            canvas.drawText("Score: " + this.score, 250, 100, paintbrush);


            if (gameOver == true) {
                // show GAME OVER in middle of screen
                canvas.drawText("GAME IS OVER!", this.screenWidth/3, this.screenHeight/2, paintbrush);

            }
            // --------------------------------
            holder.unlockCanvasAndPost(canvas);



        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {

                this.player.setyPosition(this.player.getyPosition() + 378);
                this.player.setxPosition(this.player.getxPosition() - 50);

            // update hitbox position
            player.updateHitbox();
            if(this.player.getyPosition() >= 1521)
            {
                this.player.setyPosition((this.player.getyPosition() - 378));
            }


            }




        else if (userAction == MotionEvent.ACTION_UP) {
//            this.player.setyPosition(this.player.getyPosition() - 378);
//
//            // update hitbox position
//            player.updateHitbox();
//            if(this.player.getyPosition() >= 0)
//            {
//                this.player.setyPosition((this.player.getyPosition() + 378));
//            }
//
       }

        return true;

    }

    private void spawnPlayer() {
        // put player in middle of screen --> you may have to adjust the Y position
        // depending on your device / emulator

        //player = new Player(this.context(), 50, 178);

    }


//    private void spawnEnemyShips() {
//        Random random = new Random();

//        //@TODO: Place the enemies in a random location
//        enemy = new Sprite(th);
//    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}


