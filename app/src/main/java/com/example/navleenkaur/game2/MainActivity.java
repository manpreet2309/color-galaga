package com.example.navleenkaur.game2;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {



    private GameEngine gameEngine;

    // screen size variables
    Display display;
    Point size;
    int screenHeight;
    int screenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();

        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;

        gameEngine = new GameEngine(this, screenWidth, screenHeight);
        setContentView(gameEngine);

    }


    @Override
    protected void onPause() {
        super.onPause();
        gameEngine.pauseGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameEngine.resumeGame();
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }
    

    @Override
    public boolean onFling(MotionEvent moveup, MotionEvent movedown, float up , float down) {
       boolean result = false;

       float diffy = movedown.getY() - moveup.getY();
       float diffx = moveup.getX() -  movedown.getY();
       if(Math.abs(diffx) > Math.abs(diffy)){
           if(Math.abs(diffx)> 100 && Math.abs(diffx)> 100){

               if(diffx > 0)
               {
                   onSwipeDown();

               }
               else {

                  onSwipeUp();
               }
           }
       }
        return result;
    }

    private void onSwipeDown() {
    }
    private void onSwipeUp() {
    }

}


