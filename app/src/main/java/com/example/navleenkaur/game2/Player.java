package com.example.navleenkaur.game2;

import android.content.Context;
import android.graphics.Rect;

public class Player {


    private int xPosition;
    private int yPosition;
    private int width;

    // make the hitbox
    Rect hitbox;

    // vector variables
    double xn = 0;
    double yn = 0;

    public Player(Context context, int x, int y, int imageName) {
        this.xPosition = x;
        this.yPosition = y;
        this.width = width;

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.width,
                this.yPosition + this.width
        );

    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.width;
        this.hitbox.bottom = this.yPosition + this.width;
    }


    // ---------------------------------
    // sets or gets the xd variable for this sprite
    // ---------------------------------
    public double getXn() {
        return xn;
    }

    public void setXn(double xn) {
        this.xn = xn;
    }

    public double getYn() {
        return yn;
    }

    public void setYn(double yn) {
        this.yn = yn;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
        this.updateHitbox();
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
        this.updateHitbox();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }
}

